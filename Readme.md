# A Notes taking CLI

A very simple way to manage notes via the CLI. Notes are stored in a JSON file.
Supports the following subcommands

- `add`: Add a note
- `list`: List your notes